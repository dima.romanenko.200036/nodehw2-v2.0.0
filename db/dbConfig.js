const mongoose = require('mongoose')
const CONNECTION = process.env.DB_CONNECTION_STRING
const db = mongoose.connect(CONNECTION)

db.then(() => console.log('Database connection established'))
  .catch(err => {
    console.log(err.message)
  })

module.exports = { db }
