const getCreationDate = () => {
  const date = new Date()
  return date.toISOString()
}

module.exports = getCreationDate