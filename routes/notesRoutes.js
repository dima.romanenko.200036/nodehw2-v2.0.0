const { Router } = require('express');
const {
  getUserNotes,
  deleteUserNote,
  toggleUserNote,
  createUserNote,
  getUserNote,
  updateUserNote,
} = require('../controllers/notesControllers');
const jwtValidator = require('../middlewares/tokenValidator');
const router = Router();

router.use('/', jwtValidator);
router.get('/', getUserNotes);
router.post('/', createUserNote);
router.get('/:id', getUserNote);
router.put('/:id', updateUserNote);
router.patch('/:id', toggleUserNote);
router.delete('/:id', deleteUserNote);

module.exports = router;
