const { Router } = require('express');
const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require('../controllers/usersControllers');
const jwtValidator = require('../middlewares/tokenValidator');
const router = Router();

router.use('/me', jwtValidator);
router.get('/me', getUser);
router.delete('/me', deleteUser);
router.patch('/me', changeUserPassword);

module.exports = router;
