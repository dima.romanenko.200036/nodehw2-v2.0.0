const User = require('../models/userModel');
const Note = require('../models/noteModel');
const {
  validateHashedPassword,
  hashPassword,
} = require('../tools/bcryptPassword');
const USER_SCHEMA = ['_id', 'username', 'createdDate'];

const getUser = async (req, res, next) => {
  try {
    const { _id } = req.verifiedUser;
    const user = await User.findById(_id, USER_SCHEMA);
    res.status(200).json({ user });
  } catch (error) {
    if (!error.status) {
      res.status(500).json({ message: 'Internal server error' });
    } else {
      res.status(400).json({ message: error.message });
    }
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const { _id } = req.verifiedUser;
    await User.findByIdAndDelete(_id);
    await Note.deleteMany({ userId: _id });
    res.status(200).json({ message: 'Success' });
  } catch (error) {
    if (!error.status) {
      res.status(500).json({ message: 'Internal server error' });
    } else {
      res.status(400).json({ message: error.message });
    }
  }
};

const changeUserPassword = async (req, res, next) => {
  try {
    const { _id } = req.verifiedUser;
    const { oldPassword, newPassword } = req.body;
    const user = await User.findById(_id);
    await validateHashedPassword(user.password, oldPassword);
    user.password = await hashPassword(newPassword);
    await user.save();
    res.status(200).json({ message: 'Success' });
  } catch (error) {
    if (!error.status) {
      res.status(500).json({ message: 'Internal server error' });
    } else {
      res.status(400).json({ message: error.message });
    }
  }
};

module.exports = { getUser, deleteUser, changeUserPassword };
