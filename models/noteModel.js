const { Schema, model } = require('mongoose')

const note = new Schema(
  {
    userId: {
      type: String,
      required: true
    },
    text: {
      type: String,
      required: true
    },
    createdDate:
    {
      type: String,
      required: true
    },
    completed: {
      type: Boolean,
      default: false
    }
  }
)
const Note = model('notes', note)

module.exports = Note