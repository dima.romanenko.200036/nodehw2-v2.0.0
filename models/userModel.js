const { Schema, model } = require('mongoose')

const user = new Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    createdDate:
    {
      type: String,
      required: true
    },
    token: {
      type: String,
      default: null
    }
  }
)
const User = model('users', user)

module.exports = User
