require('dotenv').config();
require('./db/dbConfig');

const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const PORT = process.env.PORT || 8080;
const app = express();

const authRouter = require('./routes/authRoutes');
const usersRouter = require('./routes/usersRoutes');
const notesRouter = require('./routes/notesRoutes');

app.use(logger('combined'));
app.use(cors());
app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

app.use((req, res) => {
  res.status(404).json({ message: 'Bad request' });
});

app.use((err, req, res, next) => {
  res.status(500).json({ message: err.message });
});

app.listen(PORT, () => {
  console.log(`Server running on port: ${PORT}`);
});
